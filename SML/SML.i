// Add necessary symbols to generated header
%{
#include <objects/Foo.hpp>
#include <objects/Shape.hpp>
#include <objects/Exc.hpp>
%}

%immutable NumSquares;
%immutable NumCircles;

// Process symbols in header
%include <std_string.i>
%include <std_list.i>
%include <objects/Foo.hpp>
%include <objects/Shape.hpp>

/*! - this instantiation uses type int */
%template(RectangleInt) Rectangle<int>;

/*! - this instantiation uses type int */
%template(MakeRectangleInt) MakeRectangle<int>;

/* A few preprocessor macros */
#define    ICONST      42
#define    FCONST      2.1828
#define    CCONST      'x'
#define    CCONST2     '\n'
#define    SCONST      "Hello World"
#define    SCONST2     "\"Hello World\""

/* This should work just fine */
#define    EXPR        ICONST + 3*(FCONST)

/* This shouldn't do anything */
#define    EXTERN      extern

/* Neither should this (BAR isn't defined) */
#define    FOO         (ICONST + BAR)

/* The following directives also produce constants */

%constant int iconst = 37;
%constant double fconst = 3.14;

%catches(int) Test::simple();
%catches(const char *) Test::message();
%catches(Exc) Test::hosed();
%catches(A*) Test::unknown();
%catches(int, const char *, Exc) Test::multi(int x);

/* Let's just grab the original header file here */
%include <objects/Exc.hpp>

%inline %{
// The -builtin SWIG option results in SWIGPYTHON_BUILTIN being defined
#ifdef SWIGPYTHON_BUILTIN
bool is_python_builtin() { return true; }
#else
bool is_python_builtin() { return false; }
#endif
%}
