#pragma once
#include <string>
#include <iostream>

class Foo {
	public:
		Foo() {}
		Foo(std::string const& s) : m_string(s) {}
		void doSomething();
		std::string m_string;
		void setSomething(std::string in_string);
};

