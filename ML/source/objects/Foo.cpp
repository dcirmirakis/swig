// Foo.cpp : Defines the functions for the static library.
//

#ifdef WIN32
	#include "platdep/pch.h"
	#include "platdep/framework.h"
#endif
#include "Foo.hpp"

void Foo::doSomething() {
    std::cout << "Foo:" << m_string << std::endl;
}

void Foo::setSomething(std::string in_string) {
	m_string = in_string; 
}
