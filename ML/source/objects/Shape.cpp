#include "Shape.hpp"
#include <stdio.h>
#define M_PI 3.14159265358979323846

/* Move the shape to a new location */
void Shape::move(double dx, double dy) {
	x += dx;
	y += dy;
}

int Shape::nshapes = 0;

Circle::Circle(double r) : radius(r) {
	NumCircles++;
}

double Circle::area() {
	return M_PI * radius * radius;
}

double Circle::perimeter() {
	return 2 * M_PI * radius;
}

Square::Square(double w) : width(w) {
	NumSquares++;
}

double Square::area() {
	return width * width;
}

double Square::perimeter() {
	return 4 * width;
}

int NumSquares = 0;
int NumCircles = 0;

Square MakeSquare(double r) {
	return Square(r);
}

Circle MakeCircle(double w) {
	return Circle(w);
}

void Foo2::enum_test(speed s) {
    if (s == IMPULSE) {
        printf("IMPULSE speed\n");
    }
    else if (s == WARP) {
        printf("WARP speed\n");
    }
    else if (s == LUDICROUS) {
        printf("LUDICROUS speed\n");
    }
    else {
        printf("Unknown speed\n");
    }
}

void enum_test(color c, Foo2::speed s) {
    if (c == RED) {
        printf("color = RED, ");
    }
    else if (c == BLUE) {
        printf("color = BLUE, ");
    }
    else if (c == GREEN) {
        printf("color = GREEN, ");
    }
    else {
        printf("color = Unknown color!, ");
    }
    if (s == Foo2::IMPULSE) {
        printf("speed = IMPULSE speed\n");
    }
    else if (s == Foo2::WARP) {
        printf("speed = WARP speed\n");
    }
    else if (s == Foo2::LUDICROUS) {
        printf("speed = LUDICROUS speed\n");
    }
    else {
        printf("speed = Unknown speed!\n");
    }
}