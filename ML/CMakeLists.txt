cmake_minimum_required(VERSION 3.14)
project(ML)
set(CMAKE_VERBOSE_MAKEFILE ON)

add_library(${PROJECT_NAME}
	${PROJECT_SOURCE_DIR}/source/platdep/framework.h
	${PROJECT_SOURCE_DIR}/source/platdep/pch.h
	${PROJECT_SOURCE_DIR}/source/platdep/pch.cpp
	${PROJECT_SOURCE_DIR}/source/objects/Foo.cpp
	${PROJECT_SOURCE_DIR}/source/objects/Foo.hpp
	${PROJECT_SOURCE_DIR}/source/objects/Shape.hpp
	${PROJECT_SOURCE_DIR}/source/objects/Shape.cpp
	${PROJECT_SOURCE_DIR}/source/objects/Exc.hpp
 )
set_target_properties(${PROJECT_NAME} 
	PROPERTIES 
		LINKER_LANGUAGE CXX
		POSITION_INDEPENDENT_CODE ON
)
target_compile_definitions(${PROJECT_NAME} 
	PRIVATE
		$<$<PLATFORM_ID:Linux>:UNIX LINUX>
		$<$<PLATFORM_ID:Windows>:_UNICODE UNICODE>
)
target_compile_options(${PROJECT_NAME} 
	PRIVATE
		$<$<CXX_COMPILER_ID:GNU>:-w>
		#$<$<CXX_COMPILER_ID:MSVC>:/W0>
	    #$<$<CXX_COMPILER_ID:MSVC>:/Gy>
		#$<$<CXX_COMPILER_ID:MSVC>:/Zc:wchar_t>
		#$<$<CXX_COMPILER_ID:MSVC>:/Zc:inline>
		#$<$<CXX_COMPILER_ID:MSVC>:/EHsc>
		$<$<CXX_COMPILER_ID:MSVC>:$<IF:$<CONFIG:Debug>,/MTd,/MT>>
)
target_include_directories(${PROJECT_NAME} 
	PRIVATE
		${PROJECT_SOURCE_DIR}/source
	PUBLIC
		${PROJECT_SOURCE_DIR}/source
)
