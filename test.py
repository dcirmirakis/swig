# file: runme.py

# This file illustrates the proxy class C++ interface generated
# by SWIG.

import PySML as ml

# ----- Object creation -----

print("Creating some objects:")
c = ml.Circle(10)

print("    Created circle" + str(c))
s = ml.Square(10)
print("    Created square", str(s))

# ----- Access a static member -----

print("\nA total of " + str(ml.cvar.Shape_nshapes) + " shapes were created")

# ----- Member data access -----

# Set the location of the object

c.x = 20
c.y = 30

s.x = -10
s.y = 5

print("\nHere is their current position:")
print("    Circle = ({x}, {y})".format(x=c.x, y=c.y))
print("    Square = ({x}, {y})".format(x=s.x, y=s.y))

# ----- Call some methods -----

print("\nHere are some properties of the shapes:")
for o in [c, s]:
    print("   " + str(o))
    print("        area      = "+str(o.area()))
    print("        perimeter = "+str(o.perimeter()))
# prevent o from holding a reference to the last object looked at
o = None

print("\nGuess I'll clean up now")

# Note: this invokes the virtual destructor
del c
del s

print(str(ml.cvar.Shape_nshapes) + "shapes remain")
print("Goodbye")

print("Creating some objects:")
c = ml.MakeCircle(10)
print("    Created circle {0}".format(str(c)))
s = ml.MakeSquare(10)
print("    Created square {0}".format(s))
r = ml.MakeRectangleInt(10, 20)
print("    Created rectangle".format(r))

print("\nHere are some properties of the shapes:")
for o in [c, s, r]:
    print("   {0}".format(o))
    print("        area      = {0}".format(o.area()))
    print("        perimeter = {0}".format(o.perimeter()))

print("\nRunning pydoc, this is the equivalent to executing: pydoc -w ./PySML.py")

import pydoc

pydoc.writedoc("PySML")

print("Open example.html in your browser to view the generated python docs")

# Print out the value of some enums
print("*** color ***")
print("    RED    ={0}".format(ml.RED))
print("    BLUE   ={0}".format(ml.BLUE))
print("    GREEN  ={0}".format(ml.GREEN))

print("\n*** Foo::speed ***")
print("    Foo_IMPULSE   ={0}".format(ml.Foo2.IMPULSE))
print("    Foo_WARP      ={0}".format(ml.Foo2.WARP))
print("    Foo_LUDICROUS ={0}".format(ml.Foo2.LUDICROUS))

print("\nTesting use of enums with functions\n")

ml.enum_test(ml.RED, ml.Foo2.IMPULSE)
ml.enum_test(ml.BLUE, ml.Foo2.WARP)
ml.enum_test(ml.GREEN, ml.Foo2.LUDICROUS)
ml.enum_test(1234, 5678)

print("\nTesting use of enum with class method")
f = ml.Foo2()

f.enum_test(ml.Foo2.IMPULSE)
f.enum_test(ml.Foo2.WARP)
f.enum_test(ml.Foo2.LUDICROUS)

t = ml.Test()
try:
    t.unknown()
except RuntimeError as e:
    print("incomplete type".format(e.args[0]))

try:
    t.simple()
except RuntimeError as e:
    print(e.args[0])

try:
    t.message()
except RuntimeError as e:
    print(e.args[0])

if not ml.is_python_builtin():
    try:
        t.hosed()
    except ml.Exc as e:
        print("{0} {1}".format(e.code, e.msg))
else:
    try:
        t.hosed()
    except BaseException as e:
        # Throwing builtin classes as exceptions not supported (-builtin
        # option)
        print(e)

for i in range(1, 4):
    try:
        t.multi(i)
    except RuntimeError as e:
        print(e.args[0])
    except ml.Exc as e:
        print(e.code, e.msg)
